import people_data from '../data/people_data.json';

export class PeopleProcessing {
    getById(id: number) {
        return people_data.find((p) => p.id === id);
    }

    getAll() {
        return people_data;
    }

    getAllPagination(size: number, page: number) {
        const initialValue = size * (page - 1);
        const endValue = initialValue + size - 1;
        return people_data.slice(initialValue, endValue);
    }
}
